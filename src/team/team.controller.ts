import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Patch
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { TeamService } from './team.service';
import { Team } from 'src/schemas/team.schema';
import {
  UpdateTeamDto,
  CreateTeamDto,
  TeamResponseDto,
  AddRemoveAdminDto
} from './dto';

@Controller('teams')
export class TeamController {
  constructor(private readonly teamService: TeamService) {}

  @Get()
  @ApiOperation({ summary: 'List of teams' })
  async findAll(): Promise<Team[]> {
    return this.teamService.findAll();
  }

  @Post()
  @ApiOperation({ summary: 'Create a team' })
  async create(@Body() teamInput: CreateTeamDto): Promise<TeamResponseDto> {
    return await this.teamService.create(teamInput);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get a team by ID' })
  async findById(@Param('id') id: string): Promise<TeamResponseDto> {
    return this.teamService.findById(id);
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update a team by ID' })
  async update(
    @Param('id') id: string,
    @Body() input: UpdateTeamDto
  ): Promise<TeamResponseDto> {
    return this.teamService.update(id, input);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a team by ID' })
  async delete(@Param('id') id: string): Promise<string> {
    return this.teamService.delete(id);
  }

  @Patch('/admins/unassign')
  @ApiOperation({ summary: 'Set one team member as an admin' })
  async unassignAdmin(
    @Body() input: AddRemoveAdminDto
  ): Promise<TeamResponseDto> {
    return this.teamService.unassignAdmin(input);
  }

  @Patch('/admins/assign')
  @ApiOperation({ summary: 'Unset one team member as an admin' })
  async assignAdmin(
    @Body() input: AddRemoveAdminDto
  ): Promise<TeamResponseDto> {
    return this.teamService.assignAdmin(input);
  }
}
