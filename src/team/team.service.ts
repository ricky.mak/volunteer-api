import { Logger, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Team, TeamDocument } from 'src/schemas/team.schema';
import {
  UpdateTeamDto,
  CreateTeamDto,
  TeamResponseDto,
  AddRemoveAdminDto
} from './dto';

@Injectable()
export class TeamService {
  constructor(
    @InjectModel(Team.name)
    private teamModel: Model<TeamDocument>
  ) {}

  private readonly logger = new Logger('Team Service');

  async findAll(): Promise<Team[]> {
    return await this.teamModel.find();
  }

  async create(teamInput: CreateTeamDto): Promise<TeamResponseDto> {
    try {
      const team = await this.teamModel.create(teamInput);
      return { team, status: 'success' };
    } catch (error) {
      this.logger.error(error);
      return { status: 'fail' };
    }
  }

  async findById(id: string): Promise<TeamResponseDto> {
    try {
      const teams = await this.teamModel.aggregate([
        {
          $match: {
            $expr: {
              $eq: ['$_id', { $toObjectId: id }]
            }
          }
        },
        {
          $lookup: {
            from: 'volunteers',
            let: { members: '$members', admins: '$admins' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $or: [
                      {
                        $in: [{ $toString: '$_id' }, '$$members']
                      }
                    ]
                  }
                }
              },
              {
                $set: {
                  isAdmin: {
                    $cond: {
                      if: {
                        $in: [{ $toString: '$_id' }, '$$admins']
                      },
                      then: true,
                      else: false
                    }
                  }
                }
              },
              {
                $unwind: '$isAdmin'
              },
              {
                $project: {
                  _id: 1,
                  name: 1,
                  gender: 1,
                  isAdmin: 1
                }
              }
            ],
            as: 'members'
          }
        },
        { $unset: 'admins' }
      ]);

      const team = teams[0];
      if (team === undefined) {
        return { status: 'not found' };
      }
      return { team, status: 'success' };
    } catch (error) {
      this.logger.error(error);
      return { status: 'fail' };
    }
  }

  async update(id: string, input: UpdateTeamDto): Promise<TeamResponseDto> {
    let team = await this.teamModel.findById(id);
    if (!team) {
      return { status: 'not found' };
    }

    if (input.name) {
      team.name = input.name;
    }

    if (input.members) {
      team.members = input.members;
    }

    await team.save();

    return { team, status: 'success' };
  }

  async delete(id: string): Promise<string> {
    const team = this.teamModel.findById(id);
    if (!team) {
      return 'not found';
    }
    await team.deleteOne();
    return 'success';
  }

  async unassignAdmin(input: AddRemoveAdminDto): Promise<TeamResponseDto> {
    try {
      const { id, adminId } = input;
      const team = await this.teamModel.findById(id);
      if (!team) {
        return { status: 'not found' };
      }

      const adminIndex: number = team.admins.indexOf(adminId);
      if (adminIndex === -1) {
        return { status: 'not found' };
      }

      team.admins.splice(adminIndex, 1);
      await team.save();

      return { team, status: 'success' };
    } catch (error) {
      return { status: 'error' };
    }
  }

  async assignAdmin(input: AddRemoveAdminDto): Promise<TeamResponseDto> {
    try {
      const { id, adminId } = input;
      const team = await this.teamModel.findById(id);
      if (!team) {
        return { status: 'not found' };
      }

      const isMember: number = team.members.indexOf(adminId);
      if (isMember === -1) {
        return { status: 'not a team member' };
      }

      const adminIndex: number = team.admins.indexOf(adminId);
      if (adminIndex !== -1) {
        return { status: 'already added' };
      }

      team.admins.push(adminId);
      await team.save();

      return { team, status: 'success' };
    } catch (error) {
      return { status: 'error' };
    }
  }
}
