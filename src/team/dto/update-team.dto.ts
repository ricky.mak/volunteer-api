import { ApiProperty } from '@nestjs/swagger';

export class UpdateTeamDto {
  @ApiProperty()
  name?: string;
  @ApiProperty()
  members?: string[];
  @ApiProperty()
  admins?: string[];
}
