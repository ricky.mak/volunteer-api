import { ApiProperty } from '@nestjs/swagger';

export class AddRemoveAdminDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  adminId: string;
}
