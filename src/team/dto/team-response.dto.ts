import { ApiProperty } from '@nestjs/swagger';
import { Team } from 'src/schemas/team.schema';

export class TeamResponseDto {
  @ApiProperty()
  team?: Team;
  @ApiProperty()
  status: string;
}
