import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type VolunteerDocument = Volunteer & Document;

@Schema({ collection: 'volunteers' })
export class Volunteer {
  @Prop({ required: true })
  name: string;
  @Prop({ required: true })
  gender: string;
}

export const VolunteerSchema = SchemaFactory.createForClass(Volunteer);
