import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TeamDocument = Team & Document;

@Schema({ collection: 'teams' })
export class Team {
  @Prop({ required: true })
  name: string;
  @Prop({ default: [] })
  members: string[];
  @Prop({ default: [] })
  admins: string[];
}

export const TeamSchema = SchemaFactory.createForClass(Team);
