import { Module } from '@nestjs/common';
import { VolunteerModule } from './volunteer/volunteer.module';
import { MongoConnectionModule } from './mongo-connection.module';
import { TeamModule } from './team/team.module';

@Module({
  imports: [MongoConnectionModule, VolunteerModule, TeamModule]
})
export class AppModule {}
