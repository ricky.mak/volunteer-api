import * as dotEnv from 'dotenv';

dotEnv.config();

const { DB_HOST, DB_PORT, DB_DATABASE, DB_USER, DB_PASSWORD } = process.env;

const ENV = { DB_HOST, DB_PORT, DB_DATABASE, DB_USER, DB_PASSWORD };

export default ENV;
