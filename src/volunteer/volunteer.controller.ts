import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { VolunteerService } from './volunteer.service';
import {
  VolunteerResponseDto,
  CreateVolunteerDto,
  UpdateVolunteerDto
} from './dto';
import { Volunteer } from 'src/schemas/volunteer.schema';

@Controller('volunteers')
export class VolunteerController {
  constructor(private readonly volunteerService: VolunteerService) {}

  @Get('/welcome')
  index(): string {
    return this.volunteerService.index();
  }

  @Get()
  @ApiOperation({ summary: 'Get list of volunteers' })
  async findAll(): Promise<Volunteer[]> {
    return this.volunteerService.findAll();
  }

  @Post()
  @ApiOperation({ summary: 'Create a volunteer' })
  async create(
    @Body() volunteerInput: CreateVolunteerDto
  ): Promise<VolunteerResponseDto> {
    return await this.volunteerService.create(volunteerInput);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get a volunteer by ID' })
  async findById(@Param('id') id: string): Promise<VolunteerResponseDto> {
    return this.volunteerService.findById(id);
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update a volunteer' })
  async update(
    @Param('id') id: string,
    @Body() input: UpdateVolunteerDto
  ): Promise<VolunteerResponseDto> {
    return this.volunteerService.update(id, input);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a volunteer' })
  async delete(@Param('id') id: string): Promise<string> {
    return this.volunteerService.delete(id);
  }
}
