import { Module } from '@nestjs/common';
import { VolunteerController } from './volunteer.controller';
import { VolunteerService } from './volunteer.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Volunteer, VolunteerSchema } from 'src/schemas/volunteer.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Volunteer.name, schema: VolunteerSchema }
    ])
  ],
  controllers: [VolunteerController],
  providers: [VolunteerService]
})
export class VolunteerModule {}
