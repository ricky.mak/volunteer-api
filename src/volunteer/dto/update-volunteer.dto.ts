import { ApiProperty } from '@nestjs/swagger';

export class UpdateVolunteerDto {
  @ApiProperty()
  name?: string;
  @ApiProperty()
  gender?: string;
}
