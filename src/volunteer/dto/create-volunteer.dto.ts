import { ApiProperty } from '@nestjs/swagger';
export class CreateVolunteerDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  gender: string;
}
