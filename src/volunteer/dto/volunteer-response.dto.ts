import { ApiProperty } from '@nestjs/swagger';
import { Volunteer } from 'src/schemas/volunteer.schema';

export class VolunteerResponseDto {
  @ApiProperty()
  volunteer?: Volunteer;
  @ApiProperty()
  status: string;
}
