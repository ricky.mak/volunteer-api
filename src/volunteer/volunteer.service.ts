import { Logger, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  VolunteerResponseDto,
  CreateVolunteerDto,
  UpdateVolunteerDto
} from './dto';
import { Volunteer, VolunteerDocument } from 'src/schemas/volunteer.schema';

@Injectable()
export class VolunteerService {
  constructor(
    @InjectModel(Volunteer.name)
    private volunteerModel: Model<VolunteerDocument>
  ) {}

  private readonly logger = new Logger('Volunteer Service');

  index(): string {
    this.logger.log('Welcome to volunteer-api');
    return 'Welcome to volunteer-api';
  }

  async findAll(): Promise<Volunteer[]> {
    return await this.volunteerModel.find();
  }

  async create(
    volunteerInput: CreateVolunteerDto
  ): Promise<VolunteerResponseDto> {
    try {
      const volunteer = await this.volunteerModel.create(volunteerInput);
      return { volunteer, status: 'success' };
    } catch (error) {
      this.logger.error(error);
      return { status: 'fail' };
    }
  }

  async findById(id: string): Promise<VolunteerResponseDto> {
    try {
      const volunteer = await this.volunteerModel.findById(id);
      return { volunteer, status: 'success' };
    } catch (error) {
      this.logger.error(error);
      return { status: 'fail' };
    }
  }

  async update(
    id: string,
    input: UpdateVolunteerDto
  ): Promise<VolunteerResponseDto> {
    let volunteer = await this.volunteerModel.findById(id);
    if (!volunteer) {
      return { status: 'not found' };
    }

    if (input.name) {
      volunteer.name = input.name;
    }
    if (input.gender) {
      volunteer.gender = input.gender;
    }

    await volunteer.save();

    return { volunteer, status: 'success' };
  }

  async delete(id: string): Promise<string> {
    const volunteer = this.volunteerModel.findById(id);
    if (!volunteer) {
      return 'not found';
    }
    await volunteer.deleteOne();
    return 'success';
  }
}
