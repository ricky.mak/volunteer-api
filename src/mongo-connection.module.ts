import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ENV } from './constants';

const DB_CONNECTION_URL = `mongodb://${ENV.DB_USER}:${ENV.DB_PASSWORD}@${ENV.DB_HOST}:${ENV.DB_PORT}/${ENV.DB_DATABASE}`;
const logger = new Logger(`=> DB Connection`);
logger.log(DB_CONNECTION_URL);

@Module({
  imports: [MongooseModule.forRoot(DB_CONNECTION_URL)]
})
export class MongoConnectionModule {}
